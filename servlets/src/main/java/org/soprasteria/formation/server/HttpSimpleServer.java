package org.soprasteria.formation.server;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.soprasteria.formation.filter.SimpleFilter;
import org.soprasteria.formation.servlet.ConstantResponseServlet;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

public class HttpSimpleServer {

	private final static Logger LOGGER = LoggerFactory
			.getLogger(HttpSimpleServer.class);
	
	/** Port for Jetty Server */
	private final static int HTTP_PORT = 8000;
	public static final String FILTER_PATH = "/filter";


	/** Jetty Sever */
	private Server httpServer;
	
	/**
	 * Start Jetty & ActiveMQ server
	 * Jetty server is used to mock ESB REST Services
	 * @throws Exception
	 */
	public void startServers() throws Exception {

		httpServer = new Server(HTTP_PORT);
		ServletContextHandler servletContext = new ServletContextHandler(ServletContextHandler.SESSIONS);
		servletContext.setContextPath("/");
		httpServer.setHandler(servletContext);
		servletContext.addServlet(new ServletHolder(new ConstantResponseServlet("Success to %s on %s")), "/");
		servletContext.addFilter(new FilterHolder(new SimpleFilter()), FILTER_PATH, EnumSet.of(DispatcherType.REQUEST));

		httpServer.start();
		LOGGER.info("Jetty Started");
		// wait 1 sec for server starting
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Stop Jetty
	 * @throws Exception
	 */
	//@After
	public void stopServers() throws Exception {
		if(httpServer != null){
			httpServer.stop();
		}
		LOGGER.info("Jetty Stopped");
	}

}
