package com.soprasteria.formation;

import com.soprasteria.formation.controller.DomoticControler;
import com.soprasteria.formation.controller.impl.DomoticControlerImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Hello world!
 *
 */
@Configuration
@ImportResource("classpath:beans.xml")
@ComponentScan(basePackages = "com.soprasteria.formation")
public class App 
{
    public static void main( String[] args )
    {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(App.class);

        DomoticControler controler = ctx.getBean(DomoticControler.class);

        controler.incrementeThermostat();
        controler.decrementeThermostat();

    }
}
