package com.soprasteria.formation.appareil;

/**
 * Created by lmarchau on 14/06/2016.
 *
 * Interface permettant de définir le fonctionnement des appareils thermostatiques
 */
public interface Variator extends Controlable {

    /**
     * incrementation de la puissance de l'appareil
     */
    void incrementeThermostat();

    /**
     * decrementation de la puissance de l'appareil
     */
    void decrementeThermostat();
}
