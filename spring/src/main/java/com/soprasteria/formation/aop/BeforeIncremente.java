package com.soprasteria.formation.aop;

import com.soprasteria.formation.appareil.AppareilThermostate;
import com.soprasteria.formation.appareil.Variator;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

/**
 * Created by lmarchau on 14/06/2016.
 */
@Aspect
public class BeforeIncremente {

    @Before(value = "incrementeThermostatPointCut()")
    public void startVariator(JoinPoint jp) {
        Variator variator = (Variator) jp.getTarget();
        if (variator instanceof AppareilThermostate && 0 == ((AppareilThermostate) variator).getThermostat()) {
            variator.demarrer();
        }
    }

    @Pointcut("execution(* com.soprasteria.formation.appareil.Variator.incrementeThermostat())")
    public void incrementeThermostatPointCut() {
    }
}
