package com.soprasteria.formation.controller;

/**
 * Created by lmarchau on 14/06/2016.
 * Controller de la Domotique
 */
public interface DomoticControler {


    void incrementeThermostat();

    void decrementeThermostat();

}
